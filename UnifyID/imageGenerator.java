import java.io.*;
import java.net.*;
import java.util.*;
import java.util.List;
import java.awt.image.BufferedImage;
import javax.imageio.*;

public class imageGenerator {

	public static void main(String[] args) throws IOException {
		int height = 128;
		int width = 128;
		int min = 0;
		int max = 256;
		
		HttpURLConnection conn = null;
		
		BufferedReader br = null;
		BufferedImage brImg = null;
		File fImg = null;
		
		List<Integer> nums = new ArrayList<>();	//to save numbers in cache instead calling API

		try {
			URL url = new URL("https://www.random.org/sequences/?min="+min+"&max="+max+"&col=1&base=10&format=plain&rnd=new");
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/plain");
			
			if(conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed to fetch numbers");
			} else {
				br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String result;
				
				while((result = br.readLine()) != null) {
					nums.add(Integer.parseInt(result));
				}
				
				brImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
				
				for(int i = 0; i < height; i++) {
					for(int j = 0 ; j< width; j++) {
						int red 	= nums.get((new Random()).nextInt(nums.size()));
						int blue 	= nums.get((new Random()).nextInt(nums.size()));
						int green 	= nums.get((new Random()).nextInt(nums.size()));
						
						int pixel = (red<<24) | (blue<<16) | (green<<8);
						brImg.setRGB(j, i, pixel);
						
					}
				}
				
				fImg = new File("rgbImage.bmp");
				ImageIO.write(brImg, "BMP", fImg);
				br.close();
			}
		} catch (Exception e) {
			throw new RuntimeException("Failed to draw image");
		} finally {
			conn.disconnect();
		}
	}
}
